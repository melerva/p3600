ZIP ?= zip -X -9

sources := $(shell find -type f -name "*.lua")

p3600.love: $(sources) assets/i assets/maps LICENSE.md
	$(MAKE) -C src -f ../Makefile pack_love
	$(ZIP) -r p3600.love LICENSE.md
	$(MAKE) -C assets -f ../Makefile add_assets

pack_love:
	$(ZIP) -r ../p3600.love .

add_assets:
	$(ZIP) -r ../p3600.love i maps
