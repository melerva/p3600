# p3600 Art

## Format

Images should be stored under `/src` as GIMP .xcf files, and exported to `/i` as .png whenever they are edited.

For the PNG files, please remove the alpha channels if there are no transparent parts, and prefer indexed colorspaces.
Set compression level to 0; they will all be added to a ZIP archive when the game is built, and compression is better that way.

## Credits

 - /i/font.png is `a4_font.tga` from Allegro 5 edited to fit LOVE2d's bitmap font format.
 - /i/font.ttf is from <https://int10h.org/oldschool-pc-fonts/fontlist/#ibm_pc_bios>.
