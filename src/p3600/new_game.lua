local newgame = function(ngchar)
  p3600.world = {
    player_control = 'PC', -- Which character is presently controllable
    chars = {
      PC = { -- this one's special; see Alec for docs
        pos = {
          area = 'forest_start', -- Map ID
          x = 13,                -- X coordinate
          y = 8,                 -- Y coordinate
          d = 2,                 -- Face direction; 0=up,1=left,2=down,3=right
        },
        dat = ngchar, -- require('p3600.world.chars[chr].dat)
        dat_mod = {}, -- if keys are here, use them instead of dat
      },
    },
    area = {
      forest_start = {
      },
    },
  }

  p3600.state = require('p3600.state.overworld')()
end


return function()
  local flags = {
    bgcolor = {0,0,0},
    items = {
      {
        '(back)',
        p3600.pop_state,
      },
      {
        'Farrin',
        function()
          newgame('p3600.chars.Farrin')
        end,
      },
    },
    y = p3600.dim.font * 3,
    x = 0,
    also_draw = function()
      love.graphics.print("Before we start, who are you?", 0, 0)
    end,
  }
  return require('p3600.state.menu')(flags)
end
