return function()
  local p3600 = {
    -- Change this whenever you mess with the save data format.
    -- If the game tries to load data and save_version is greater than this,
    -- it refuses. If save_version is less than this, it is assumed that the
    -- loader will automatically update it or display an error as relevant.
    version = 0,

    -- Screen and texture dimensions.
    -- These values can be changed, as long as the images themselves are
    -- edited accordingly.
    --
    -- All sprites are assumed to be square, but the screen size can be
    -- whatever.
    dim = {
      screenw = 800,
      screenh = 600,
      sprites = 16,
      spritel = 32,
      font = 16,
    },

    -- The main buffer.
    gbuf = nil,

    -- The main font. Must be monospace and square.
    font = nil,

    -- This prevents the screen from being redrawn if nothing has changed.
    -- Setting it to true causes a redraw in the main loop.
    display_redraw = true,
    -- Ditto, but for the buffer only. Implied by the above.
    display_changed = true,

    -- The game state. Contains callbacks and stuff.
    state = {},
    state_stack = {},

    push_state = nil,
    pop_state = nil,
  }

  p3600.push_state = function()
    p3600.state_stack[#p3600.state_stack + 1] = p3600.state
  end

  p3600.pop_state = function()
    p3600.state = p3600.state_stack[#p3600.state_stack]
    table.remove(p3600.state_stack)
    p3600.display_redraw = true
  end

  return p3600
end
