-- /r/restofthefuckingowl
return function(args)
  local this = require('p3600.state')()
  this.dat = {}

  this.dat.ch = p3600.world.chars[p3600.world.player_control]

  this.dat.map = require('maps.' .. this.dat.ch.pos.area)
  assert(this.dat.map.orientation == "orthogonal")
  assert(this.dat.map.renderorder == "right-down")

  this.dat.chars = {}
  for k,v in pairs(p3600.world.chars) do
    if (v.pos.area == this.dat.ch.pos.area) then
      this.dat.chars[k] = v
    end
  end

  -- TODO: see if the performance benefit of SpriteBatch is bigger than the
  -- ability to scroll
  this.dat.tiles = {}
  for k,v in pairs(this.dat.map.tilesets) do
    assert(v.spacing == 0)
    assert(v.margin == 0)
    local img = love.graphics.newImage('/i/tiles/' .. v.name .. '/tiles.png')
    for i=1, v.tilecount do
      local x = ((i % v.columns) - 1) * v.tilewidth
      local y = math.floor(i / v.columns) * v.tileheight
      this.dat.tiles[i + (v.firstgid - 1)] = {
        t = img,
        q = love.graphics.newQuad(x, y, v.tilewidth, v.tileheight, v.imagewidth, v.imageheight),
      }
    end
  end

  this.draw = function()
    love.graphics.clear({0,0,0})

    for k,layer in ipairs(this.dat.map.layers) do
      for y=1, layer.height do
        for x=1, layer.width do
          local tileid = layer.data[x + (layer.width * (y - 1))]
          if (tileid ~= 0) then
            love.graphics.draw(this.dat.tiles[tileid].t, this.dat.tiles[tileid].q, (x - 1) * this.dat.map.tilewidth, (y - 1) * this.dat.map.tileheight)
          end
        end
      end
    end
  end

  p3600.display_redraw = true

  return this
end
