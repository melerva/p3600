return function()
  -- TODO: check for presence of save data

  local flags = {
    bg = love.graphics.newImage('i/title.png'),
    bgcolor = {0,0,0},
    items = {
      {
        'New Game',
        function()
          p3600.push_state()
          p3600.state = require('p3600.new_game')()
        end,
      },
      {
        'Load Game',
        disabled = true,
      },
      {
        'Settings',
        disabled = true,
      },
      {
        'Exit',
        love.event.quit,
      },
    },
    y = p3600.dim.font * 5,
    x = (p3600.dim.screenw / 2) - ((p3600.dim.font * 11) / 2),
    also_draw = function()
      love.graphics.print("Version 0", 0, p3600.dim.screenh - p3600.dim.font)
    end,
  }
  return require('p3600.state.menu')(flags)
end
