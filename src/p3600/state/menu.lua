return function(args)
  local this = require('p3600.state')()

  assert(args.items)
  assert(#args.items > 0)

  this.dat = {
    bg = args.bg,
    selection = 1,
  }

  this.bgcolor = args.bgcolor

  this.draw = function()
    if (this.dat.bg) then
      love.graphics.draw(this.dat.bg)
    else
      love.graphics.clear(args.bgcolor or {0,0,0})
    end

    for k,v in ipairs(args.items) do
      if (v.disabled) then
        love.graphics.print({{0.7,0.7,0.7}, v[1]}, args.x + p3600.dim.font, args.y + (p3600.dim.font * (k - 1)))
      else
        love.graphics.print({{1,1,1}, v[1]}, args.x + p3600.dim.font, args.y + (p3600.dim.font * (k - 1)))
      end
    end

    love.graphics.print(">", args.x, args.y + (p3600.dim.font * (this.dat.selection - 1)))

    if (args.also_draw) then
      return args.also_draw()
    end
  end

  p3600.display_redraw = true

  do
    local keyh = {
      ['up'] = function()
        if (this.dat.selection > 1) then
          this.dat.selection = this.dat.selection - 1
        else
          this.dat.selection = #args.items
        end
        p3600.display_redraw = true
      end,
      ['down'] = function()
        if (this.dat.selection < #args.items) then
          this.dat.selection = this.dat.selection + 1
        else
          this.dat.selection = 1
        end
        p3600.display_redraw = true
      end,
      ['return'] = function()
        if (type(args.items[this.dat.selection][2]) == 'function') then
          return args.items[this.dat.selection][2]()
        end
      end,
    }
    this.keydown = function(key, scancode, isrepeat)
      if (keyh[key]) then
        return keyh[key]()
      end
    end
  end

  return this
end
