love.keypressed = function(...)
  if (p3600.state.keydown) then
    return p3600.state.keydown(...)
  end
end

-- Redraw if the WM is messed with (otherwise weird stuff happens)
love.resize = function()
  p3600.display_changed = true
end
love.focus = love.resize
love.mousefocus = love.resize
love.visible = love.resize

-- Do all the setup, then return a function to be looped
function love.run()
  -- Make all future additions to package.loaded unload when no longer needed.
  package.nogc = {}
  for k,v in pairs(package.loaded) do
    package.nogc[k] = v
  end
  setmetatable(package.loaded, {__mode = 'v'})

  p3600 = require('p3600')()
  love.graphics.setDefaultFilter('nearest', 'nearest', 0)
  love.graphics.setLineJoin('miter')
  p3600.gbuf = love.graphics.newCanvas(p3600.dim.screenw, p3600.dim.screenh)
  p3600.font = love.graphics.newFont("i/font.ttf", p3600.dim.font)
  --p3600.font = love.graphics.newImageFont("i/font.png", " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~")
  love.graphics.setFont(p3600.font)

  p3600.state = require('p3600.state.title')()

  love.graphics.origin()
  love.timer.step()

  return function()
    local dt = 0
    love.event.pump()
    for name, a,b,c,d,e,f in love.event.poll() do
      if name == "quit" then
        if not love.quit or not love.quit() then
          return a or 0
        end
      end
      love.handlers[name](a,b,c,d,e,f)
    end

    dt = love.timer.step()

    if (p3600.state.update) then
      p3600.state.update(dt)
    end

    if (love.graphics.isActive()) then
      if (p3600.display_redraw) then
        if (p3600.state.draw) then
          love.graphics.setCanvas(p3600.gbuf)
          p3600.state.draw()
        end
        p3600.display_redraw = false
        p3600.display_changed = true
      end

      if (p3600.display_changed) then
        love.graphics.setCanvas()
        love.graphics.clear(p3600.state.bgcolor or {0,0,0})

        do
          -- TODO: if sw and sh are equivalent to internal dimensions,
          -- this math is redundant and shouldn't be calculated for that frame
          -- ... unless the if statement is slower than the math and assignment
          local d = p3600.dim
          local sw = love.graphics.getWidth()
          local sh = love.graphics.getHeight()
          local sfw = sw / d.screenw
          local sfh = sh / d.screenh
          local sf = math.min(sfh, sfw)
          --local dw = d.screenw * sf
          --local dh = d.screenh * sf
          local dx = (sw / 2) - ((d.screenw * sf) / 2);
          local dy = (sh / 2) - ((d.screenh * sf) / 2);
          love.graphics.draw(p3600.gbuf, dx, dy, 0, sf, sf)
          --al_draw_scaled_bitmap(p3600_gbuf, 0, 0, P3600_SCREENDIM_W,
           --P3600_SCREENDIM_H, dx, dy, dw, dh, 0);
         end

        love.graphics.present()
        p3600.display_changed = false
      end
    end

    --[[ show off how much memory is saved
    do
      local a = collectgarbage('count')
      if (p3600.gc ~= a) then
        p3600.gc = a
        print(a)
      end
    end
    ]]
    collectgarbage('step') -- I'm anal about my memory usage
    love.timer.sleep(p3600.state.tick_time or 0.001) -- TODO: this might actually lag
  end
end
