local one_of = function(v, ...)
  local l = select('#', ...)
  for i = 1, l, 1 do
    local n = select(i, ...)
    if (v == n) then
      return true
    end
  end
  return false
end

-- convenience methods (use with 'misc.so_meta')
getmetatable("").one_of = one_of -- strings
getmetatable(0).one_of = one_of -- numbers

return one_of
