local dsetmetatable = require('debug').setmetatable

-- Sets weak metatables for most types (except tables and userdata),
-- allowing modules to add functions as members.
-- Since the metatables are weak, members will be unloaded
-- when the modules are.
return function()
  local mode_weak = {__mode = 'v'}

  do -- string
    local a_string = ""

    local mt = getmetatable(a_string)
    if (mt == nil) then
      mt = {}
      mt.__index = mt
      dsetmetatable(a_string, mt)
    end

    setmetatable(mt, mode_weak)
  end

  do -- number
    local a_number = 42

    local mt = getmetatable(a_number)
    if (mt == nil) then
      mt = {}
      mt.__index = mt
      dsetmetatable(a_number, mt)
    end

    setmetatable(mt, mode_weak)
  end

  do -- boolean
    local a_boolean = true

    local mt = getmetatable(a_boolean)
    if (mt == nil) then
      mt = {}
      mt.__index = mt
      dsetmetatable(a_boolean, mt)
    end

    setmetatable(mt, mode_weak)
  end

  do -- function
    local a_function = function()
    end

    local mt = getmetatable(a_function)
    if (mt == nil) then
      mt = {}
      mt.__index = mt
      dsetmetatable(a_function, mt)
    end

    setmetatable(mt, mode_weak)
  end
end
