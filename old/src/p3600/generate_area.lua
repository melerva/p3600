return function(name)
  local map = assert(loadfile('/p3600/generator/skeleton.lua'))()

  local params
  do
    local area_name = name:gmatch('[^_]+')()
    local x
    local y
    do
      local left = name:sub(#area_name + 1 + 1)
      x = left:gmatch('[^_]+')()
      y = left:sub(#x + 1 + 1)
    end
    params = p3600.generator.params[area_name](tonumber(x), tonumber(y))
  end

  map.tilesets[1] = p3600.generator.tilesets[params.tileset]
  map.height = params.height
  map.width = params.width
  map.layers[1].height = params.height
  map.layers[1].width = params.width
  map.layers[2].height = params.height
  map.layers[2].width = params.width
  map.layers[3].height = params.height
  map.layers[3].width = params.width

  do
    local i = 1

    for y = 1, params.height, 1 do
      for x = 1, params.width, 1 do
        map.layers[1].data[i] = params.bg[y][x] + 1
        map.layers[2].data[i] = params.fg[y][x] + 1
        map.layers[3].data[i] = params.tiletypes[y][x] + 1
        i = i + 1
      end
    end
  end

  map.layers[5].properties.top = params.exits.top
  map.layers[5].properties.bottom = params.exits.bottom
  map.layers[5].properties.left = params.exits.left
  map.layers[5].properties.right = params.exits.right

  map.layers[4].objects[1].x = (params.default_entrance.player.x * 16) - 1
  map.layers[4].objects[1].y = (params.default_entrance.player.y * 16) - 1
  map.layers[4].objects[1].properties.dir = params.default_entrance.player.dir
  map.layers[4].objects[2].x = (params.default_entrance.followers.x * 16) - 1
  map.layers[4].objects[2].y = (params.default_entrance.followers.y * 16) - 1
  map.layers[4].objects[2].properties.dir =
   params.default_entrance.followers.dir

  local f
  do
    local filename = '/save/'..p3600.gstate.entity[0].name..'/p3600/area/'
    love.filesystem.createDirectory(filename)
    f = love.filesystem.newFile(filename..name..'.lua', 'w')
  end

  f:write('return ')
  p3600.serialize(f, map, '  ')
  f:write("\n")
  f:flush()
  f:close()
end
