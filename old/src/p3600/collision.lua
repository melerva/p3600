local abs = require('math').abs
local floor = require('math').floor

return function(_e, check_for)
  check_for = check_for or {
    entities = true,
    map = true,
  }

  local r = {}

  if (check_for.entity) then -- entities
    local x = _e.pos.x
    local y = _e.pos.y
    local ae = p3600.get_entities_in_area(_e.pos.area)

    for _, e in pairs(ae) do
      if (e ~= _e) then
        if (abs(y - e.pos.y) < 1) and (abs(x - e.pos.x) < 1) then
          r.entity = true
          break
        end
      end
    end
  end

  if (check_for.map) then -- map
    local f
    f = function(entity, recurse)
      local x = floor(entity.pos.x)
      local y = floor(entity.pos.y)

      local valid = false

      if (recurse == 0) then
        valid = true
      else
        local x_inc = (floor(entity.pos.x + 0.9) > x)
        local y_inc = (floor(entity.pos.y + 0.9) > y)

        if (recurse == 1) then
          if (x_inc) then
            x = x + 1
            valid = true
          end
        elseif (recurse == 2) then
          if (y_inc) then
            y = y + 1
            valid = true
          end
        else
          if (x_inc and y_inc) then
            x = x + 1
            y = y + 1
            valid = true
          end
        end
      end

      if (valid) then
        local tiletype

        if (p3600.state.map ~= nil) then
          tiletype = p3600.state.map.tiletype
        else
          tiletype = p3600.area[entity.pos.area].data.tiletypes
        end

        if (tiletype[y] and tiletype[y][x] == 1) then
          r.obstacle = true
        end
      end

      if (recurse < 3) then
        return f(entity, recurse + 1)
      end
    end

    f(_e, 0)
  end

  return r
end
