local sti = require('sti')

return function(name)
  local map

  do
    local mapdata = sti('/p3600/area/'..name..'.lua')

    map = {
      name = name,
      width = mapdata.layers['tiletypes'].width,
      height = mapdata.layers['tiletypes'].height,
      tiletype = {},
      data = mapdata,
      entrances = {},
      exits = {},
      vx = 0,
      vy = 0,
    }

    for y, r in pairs(mapdata.layers['tiletypes'].data) do
      map.tiletype[y] = {}
      for x, t in pairs(r) do
        map.tiletype[y][x] = t.id
      end
    end

    map.height = #map.tiletype
    map.width = #map.tiletype[1]

    do
      local e = mapdata:getLayerProperties('exits')
      map.exits['top'] = e['top']
      map.exits['bottom'] = e['bottom']
      map.exits['left'] = e['left']
      map.exits['right'] = e['right']
    end

    do
      local l = mapdata.layers['exits'].objects
      for _, o in pairs(l) do
        local y = (o.y / 16) + 1
        if (map.exits[y] == nil) then
          map.exits[y] = {}
        end
        map.exits[y][(o.x / 16) + 1] = o.name
      end
    end

    if (mapdata.layers['entrances'] ~= nil) then
      for _, e in pairs(mapdata.layers['entrances'].objects) do
        local id = e.name:gmatch('[^/]+')()
        local subv = e.name:gsub('.*/', '')

        if (map.entrances[id] == nil) then
          map.entrances[id] = {}
        end

        map.entrances[id][subv] = {
          x = (e.x / 16) + 1,
          y = (e.y / 16) + 1,
          dir = e.properties.dir,
        }
      end
    end
  end

  if (love.filesystem.exists('/p3600/area/'..name..'_data.lua')) then
    local data = assert(loadfile('/p3600/area/'..name..'_data.lua'))()
    map.onload = data.onload

    if (data.set_tiletypes ~= nil) then
      data.set_tiletypes(map, map.tiletype)
    end
  end

  return map
end
