return function(eid, x, y)
  local followers = p3600.gstate.entity[eid].followers
  if (followers ~= nil) then
    for i, e in pairs(followers) do
      if
       (p3600.gstate.entity[e].pos.area ~= p3600.gstate.entity[eid].pos.area)
      then
        p3600.gstate.entity[e].pos = {
          x = x,
          y = y,
          area = p3600.gstate.entity[0].pos.area,
        }

        if (p3600.gstate.entity[e].following ~= nil) then
          p3600.gstate.entity[e].following.prev = nil
        end
      end
    end
  end
end
