-- Gets the tiletypes of any area, even if it isn't active.
return function(area)
  if (p3600.state.map ~= nil) then
    if (p3600.state.map.tiletype ~= nil) then
      if (p3600.state.map.name == area) then
        return p3600.state.map.tiletype
      end
    end
  end

  return p3600.display.make_map(area).tiletype -- This needs to be optimized.
end
