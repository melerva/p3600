return function()
  p3600.gstate.entity[1].is_known = true

  return p3600.display.dialog{
    entity = 1,
    target_player = true,
    time_left = 10,

    text = {
      "I'm a shapeshifter!",
      'That should be obvious.',
    },
  }
end
