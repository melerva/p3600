return function()
  local t = {
    lock = true,
    entity = 1,
    target_player = true,

    text = {},
    choices = {
      {
        label = 'I have no idea what happened.',
        action = p3600.sp_entity[1].dialog.init_clearing[3],
      },
    },
  }

  t.text[#t.text + 1] = 'Gee, I dunno!'
  if not (p3600.gstate.entity[0].race:one_of(11, 17, 18)) then
    t.text[#t.text + 1] = 'I just found you laying here,'
    t.text[#t.text + 1] = "and you didn't have a pulse!"
    t.choices[#t.choices + 1] = {
      label = 'How did you revive me?',
      action = p3600.sp_entity[1].dialog.init_clearing[4],
    }
  else
    t.text[#t.text + 1] = 'I just found you laying here,'
    t.text[#t.text + 1] = "and you weren't moving!"
  end

  p3600.display.dialog(t)
end
