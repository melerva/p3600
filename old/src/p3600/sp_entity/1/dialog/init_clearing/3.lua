return function()
  local t = {
    entity = 1,
    target_player = true,
    time_left = 10,
    text = {},
  }

  t.text[#t.text + 1] = "Anyway, I'm bored and you seem fun,"
  t.text[#t.text + 1] = "so I'm gonna follow you until I find"
  t.text[#t.text + 1] = 'something more interesting.'

  return p3600.display.dialog(t)
end
