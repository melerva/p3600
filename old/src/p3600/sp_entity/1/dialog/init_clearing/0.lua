return function()
  return p3600.display.dialog{
    target_player = true,
    entity = 1,
    lock = true,

    text = {
      'Hi there!',
      'You were out for quite a while;',
      "I thought I'd lost you!",
    },

    choices = {
      {
        label = 'Where am I?',
        action = p3600.sp_entity[1].dialog.init_clearing[1],
      },

      {
        label = 'What happened?',
        action = p3600.sp_entity[1].dialog.init_clearing[2],
      },
    },
  }
end
