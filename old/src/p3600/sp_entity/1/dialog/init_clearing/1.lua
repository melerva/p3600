return function()
  p3600.display.dialog{
    lock = true,
    entity = 1,
    target_player = true,

    text = {
      "You don't remember?!",
      '...',
      "Well, I'm lost, so I guess",
      'you are too. ;P',
      '',
      "We're in a HUGE forest.",
    },

    choices = {
      {
        label = '(back)',
        action = p3600.sp_entity[1].dialog.init_clearing[0],
      }
    },
  }
end
