return function()
  p3600.display.dialog{
    lock = true,
    entity = 1,
    target_player = true,

    text = {
      'Are you insane?!',
      'Awesome, so am I!',
      '',
      "Let's get going then!",
    },

    choices = {
      {
        label = 'Um...',
        action = function()
          -- TODO: unarmed combat skills
          p3600.state.dialog = nil
        end,
      },
    },
  }
end
