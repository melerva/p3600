return function()
  p3600.display.dialog{
    lock = true,
    entity = 1,
    target_player = true,

    text = {
      'Okey dokey!',
      '...',
      "No, those aren't blood stains.",
      'Probably.',
    },

    choices = {
      {
        label = 'Feels like rust...',
        action = function()
          p3600.state.dialog = nil
          local sword = p3600.gstate.entity[1]:search_inv('sao_longsword')
          sword = p3600.gstate.entity[1]:take(sword)
          p3600.gstate.entity[0]:give(sword)
        end,
      },
    },
  }
end
