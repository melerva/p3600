return {
  onload = function(map, restore)
    if not (restore) then
      if (p3600.gstate.west_forest_town == nil) then
        p3600.gstate.west_forest_town = {
          gate_open = false,
        }
      end

      local guard_1 = p3600.Entity{
        race = 0,
      }
      p3600.gstate.entity[#p3600.gstate.entity + 1] = guard_1
      guard_1.eid = #p3600.gstate.entity
      local guard_2 = p3600.Entity{
        race = 0,
      }
      p3600.gstate.entity[#p3600.gstate.entity + 1] = guard_2
      guard_2.eid = #p3600.gstate.entity

      local g1_dat
      local g2_dat

      do
        local l = map.data.layers['misc'].objects
        for _, o in pairs(l) do
          if (o.name == 'guard_1') then
            g1_dat = {
              x = (o.x / 16) + 1,
              y = (o.y / 16),
              dir = o.properties.dir,
            }
          elseif (o.name == 'guard_2') then
            g2_dat = {
              x = (o.x / 16) + 1,
              y = (o.y / 16),
              dir = o.properties.dir,
            }
          end

          if (g1_dat and g2_dat) then
            break
          end
        end
      end

      guard_1.pos = {
        x = g1_dat.x,
        y = g1_dat.y,
        area = 'west_forest_town_gate',
      }
      guard_1.dir = g1_dat.dir

      guard_2.pos = {
        x = g2_dat.x,
        y = g2_dat.y,
        area = 'west_forest_town_gate',
      }
      guard_2.dir = g2_dat.dir
    end

    local ih = function(_)
      p3600.push_state()
      if (p3600.gstate.west_forest_town.gate_open) then
        p3600.display.dialog{
          text = {
            'Guard:',
            '  Welcome.',
          },
          choices = {
            {
              label = '(back)',
              action = function()
                p3600.display.end_dialog()
                p3600.pop_state()
              end,
            },
          },
        }
      else
        local endd = {
          label = '(back)',
          action = function()
            p3600.display.end_dialog()
            p3600.pop_state()
          end,
        }

        local t =  {
          'Guard:',
          "  I'm sorry stranger, but a monster was recently",
          '  spotted close to the walls. Until we recieve',
          '  the all-clear, you must wait outside.',
        }

        if
         (p3600.gstate.entity[1]) and
         (p3600.gstate.entity[1].following) and
         (p3600.gstate.entity[1].following.id == 0)
        then
          t[#t + 1] = ''
          t[#t + 1] = 'Saoirse:'
          t[#t + 1] = '  ...'
        end

        p3600.display.dialog{
          text = t,
          choices = {
            endd,
            {
              label = 'What if the monster gets me?',
              action = function()
                local t = {
                  'Guard:',
                  '  One of us will defend you, of course!',
                  "  We aren't heartless monsters!",
                }

                if
                 (p3600.gstate.entity[1]) and
                 (p3600.gstate.entity[1].following) and
                 (p3600.gstate.entity[1].following.id == 0)
                then
                  t[#t + 1] = ''
                  t[#t + 1] = 'Saoirse:'
                  t[#t + 1] = '  (By the way, this form has a functional'
                  t[#t + 1] = '  heart, and if I was gonna "get" you, I'
                  t[#t + 1] = "  would've done it in the forest.)"
                end

                p3600.display.dialog{
                  text = t,
                  choices = {
                    endd,
                  },
                }
              end,
            },
            {
              label = 'Do I look like a monster to you?',
              action = function()
                p3600.state_stack.state.can_ask_about_animosity = true

                local t = {
                  'Guard:',
                  '  Actually, it was a shapeshifter, so you',
                  '  might be the monster.',
                  '  Shapeshifters get bored quickly, so if you',
                  "  are one, you'll leave soon enough.",
                  "  That's the protocol for dealing with them:",
                  '  just wait a few hours.',
                }

                if
                 (p3600.gstate.entity[1]) and
                 (p3600.gstate.entity[1].following) and
                 (p3600.gstate.entity[1].following.id == 0)
                then
                  t[#t + 1] = ''
                  t[#t + 1] = 'Saoirse:'
                  t[#t + 1] = "  (He's right, any other day I'd be gone"
                  t[#t + 1] = '  by now. But this is important.)'
                end

                p3600.display.dialog{
                  text = t,
                  choices = {
                    endd,
                  },
                }
              end,
            },
          },
        }
      end
    end

    p3600.state.interact_handlers = {
      [#p3600.gstate.entity] = ih,
      [#p3600.gstate.entity - 1] = ih,
    }
  end,

  set_tiletypes = function(map, tiletypes)
    if
     p3600.gstate.west_forest_town and
     p3600.gstate.west_forest_town.gate_open
    then
      local gate

      do
        local l = map.data.layers['misc'].objects
        for _, o in pairs(l) do
          if (o.name == 'gate') then
            gate = o
            break
          end
        end
      end

      local min_x = (gate.x / 16) + 1
      local max_x = min_x + ((gate.width / 16) - 1)
      local min_y = (gate.y / 16) + 1
      local max_y = min_y + ((gate.height / 16) - 1)

      for y = min_y, max_y, 1 do
        for x = min_x, max_x, 1 do
          tiletypes[y][x] = 0
        end
      end

      map.data:removeLayer('gate')
    end
  end,
}
