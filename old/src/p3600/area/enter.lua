local ceil = require('math').ceil
local floor = require('math').floor

return function(name, edge, ...)
  p3600.unuse_sprites()

  p3600.clear_love_callbacks()
  p3600.slowness = 0.01

  if not (love.filesystem.exists('/p3600/area/'..name..'.lua')) then
    p3600.generate_area(name)
  end

  p3600.state = {
    map = p3600.display.make_map(name),
    re = p3600.display.render_entity,
    k = p3600.reverse_aa(p3600.kb.w),
    update_player = p3600.update_player,
    ue = p3600.update_entity,
    changed = true,
    can_save = true,
  }

  local prev_area = p3600.gstate.entity[0].pos.area

  if (p3600.state.map.onload ~= nil) then
    p3600.state.map.onload(p3600.state.map, (prev_area == name), ...)
    p3600.state.map.onload = nil
  end

  if (prev_area ~= name) then
    do
      local ppos = p3600.gstate.entity[0].pos

      ppos.area = name

      if not (edge) then
        local entrance = p3600.state.map.entrances[prev_area]
        if (entrance == nil) then
          entrance = p3600.state.map.entrances.default
        end
        ppos.x = entrance.player.x
        ppos.y = entrance.player.y
        p3600.pull_followers(0, entrance.followers.x, entrance.followers.y)
      elseif (edge == 'top') then
        ppos.y = p3600.state.map.data.height
        p3600.pull_followers(0, ppos.x, ppos.y)
      elseif (edge == 'bottom') then
        ppos.y = 1
        p3600.pull_followers(0, ppos.x, ppos.y)
      elseif (edge == 'left') then
        ppos.x = p3600.state.map.data.width
        p3600.pull_followers(0, ppos.x, ppos.y)
      else -- (edge == 'right')
        ppos.x = 1
        p3600.pull_followers(0, ppos.x, ppos.y)
      end
    end

    p3600.clean_other_areas(name)
  end

  p3600.state.active_entities = p3600.get_entities_in_area(name)

  do
    local u = p3600.use_sprites
    for eid, v in pairs(p3600.state.active_entities) do
      u(eid)
    end
  end

  p3600.keypressed = function(key)
    if (p3600.state.dialog and p3600.state.dialog.lock) then
      local tbl = {
        ['up'] = function()
          if (#p3600.state.dialog.text <= 3) then
            if (p3600.state.dialog.selection > 1) then
              p3600.state.dialog.selection = p3600.state.dialog.selection - 1
              p3600.state.changed = true
            end
          end
        end,

        ['down'] = function()
          if (#p3600.state.dialog.text <= 3) then
            if (p3600.state.dialog.selection < #p3600.state.dialog.choices) then
              p3600.state.dialog.selection = p3600.state.dialog.selection + 1
              p3600.state.changed = true
            end
          else
            require('table').remove(p3600.state.dialog.text, 1)
            p3600.state.changed = true
          end
        end,

        ['select'] = function()
          if (#p3600.state.dialog.text <= 3) then
            p3600.state.changed = true
            p3600.state.dialog.choices[p3600.state.dialog.selection].action()
          end
        end,
      }
      tbl['right'] = tbl['select']

      if (p3600.kb.m[key] ~= nil) then
        if (tbl[p3600.kb.m[key]] ~= nil) then
          tbl[p3600.kb.m[key]]()
        end
      end
    else
      local tbl = {
        ['pause'] = function()
          p3600.state.changed = true
          return p3600.pause(p3600.state.can_save)
        end,

        ['interact'] = function()
          local tx = p3600.gstate.entity[0].pos.x
          local ty = p3600.gstate.entity[0].pos.y
          local ta = p3600.gstate.entity[0].pos.area

          if (p3600.gstate.entity[0].dir == 0) then
            ty = ty + 1
          elseif (p3600.gstate.entity[0].dir == 1) then
            tx = tx - 1
          elseif (p3600.gstate.entity[0].dir == 2) then
            ty = ty - 1
          else -- (p3600.gstate.entity[0].dir == 3)
            tx = tx + 1
          end

          local e = p3600.get_entity_at(tx, ty, 0.3, ta)

          if (e) then
            p3600.state.changed = true
            return p3600.interact(e)
          end
        end,

        ['inventory'] = function()
          p3600.state.changed = true
          p3600.push_state()
          return p3600.inventory()
        end,
      }

      if (p3600.kb.w[key] ~= nil) then
        if (tbl[p3600.kb.w[key]] ~= nil) then
          return tbl[p3600.kb.w[key]]()
        end
      end
    end
  end

  p3600.update = function(dt)
    p3600.state.map.data:update(dt)

    do
      local already_did_dialog_time = false

      ::handle_dialog::
      if (p3600.state.dialog) then
        if (p3600.state.dialog.lock) then
          return
        elseif not (already_did_dialog_time) then
          already_did_dialog_time = true
          p3600.state.dialog.time_left = p3600.state.dialog.time_left - dt
          if (p3600.state.dialog.time_left <= 0) then
            local callback = p3600.state.dialog.on_end
            p3600.state.dialog = nil
            p3600.state.changed = true

            if (callback) then
              callback()
            end

            goto handle_dialog
          end
        end
      end
    end

    p3600.gstate.to_next_day = p3600.gstate.to_next_day - dt
    if (p3600.gstate.to_next_day <= 0) then
      p3600.gstate.to_next_day = ((24 * 60) * 60) / 2
      print('A DAY HAS PASSED') -- TODO: new day
    end

    p3600.state.update_player(dt)
    do
      local pcx = ceil(p3600.gstate.entity[0].pos.x)
      local pcy = ceil(p3600.gstate.entity[0].pos.y)

      if
       (p3600.state.map.exits[pcy]) and
       (p3600.state.map.exits[pcy][pcx])
      then
        return p3600.area.enter(p3600.state.map.exits[pcy][pcx])
      end

      if
       (p3600.state.map.exits.right) and
       (pcx > p3600.state.map.width)
      then
        return p3600.area.enter(p3600.state.map.exits.right, 'right')
      end

      if
       (p3600.state.map.exits.bottom) and
       (pcy > p3600.state.map.height)
      then
        return p3600.area.enter(p3600.state.map.exits.bottom, 'bottom')
      end

      pcx = floor(p3600.gstate.entity[0].pos.x)
      pcy = floor(p3600.gstate.entity[0].pos.y)

      if
       (p3600.state.map.exits[pcy]) and
       (p3600.state.map.exits[pcy][pcx])
      then
        return p3600.area.enter(p3600.state.map.exits[pcy][pcx])
      end

      if (p3600.state.map.exits.top) and (pcy < 1) then
        return p3600.area.enter(p3600.state.map.exits.top, 'top')
      end

      if (p3600.state.map.exits.left) and (pcx < 1) then
        return p3600.area.enter(p3600.state.map.exits.left, 'left')
      end
    end
    for eid, v in pairs(p3600.state.active_entities) do
      p3600.state.ue(eid, v, dt)
    end
  end

  do
    local layer = p3600.state.map.data:addCustomLayer('entity', 3)

    function layer:update(dt)
    end

    function layer:draw()
      if (p3600.state.changed) then
        for eid, v in pairs(p3600.state.active_entities) do
          p3600.state.re(v)
        end
      end
    end
  end

  p3600.draw = function()
    if (p3600.state.changed) then
      do -- update x
        local x = p3600.state.map.vx
        local nx = floor((p3600.gstate.entity[0].pos.x - 1) * 16) - (320 / 2)

        if ((nx + 320) > (p3600.state.map.width * 16)) then
          nx = (p3600.state.map.width * 16) - 320
        end

        if (nx < 0) then
          nx = 0
        end

        if (nx ~= x) then
          p3600.state.map.vx = nx
        end
      end

      do -- update y
        local y = p3600.state.map.vy
        local ny = floor((p3600.gstate.entity[0].pos.y - 1) * 16) - (240 / 2)

        if ((ny + 240) > (p3600.state.map.height * 16)) then
          ny = (p3600.state.map.height * 16) - 240
        end

        if (ny < 0) then
          ny = 0
        end

        if (ny ~= y) then
          p3600.state.map.vy = ny
        end
      end

      love.graphics.push()

      p3600.state.map.data:setDrawRange(p3600.state.map.vx * -1,
                                        p3600.state.map.vy * -1,
                                        320, 240)
      love.graphics.translate(p3600.state.map.vx * -1, p3600.state.map.vy * -1)

      love.graphics.clear(love.graphics.getBackgroundColor())

      p3600.state.map.data:draw()

      love.graphics.pop()

      if (p3600.state.dialog) then
        love.graphics.setColor(255, 242, 230)
        love.graphics.rectangle('fill', 0, 0, 320, (8 * 3) + 4, 5, 5)
        love.graphics.setColor(0, 0, 0)
        love.graphics.rectangle('line', 0, 0, 320, (8 * 3) + 4, 5, 5)

        love.graphics.setColor(255, 0, 0) -- TODO: portrait goes here
        love.graphics.rectangle('line', 2, 2, 24, 24)

        love.graphics.setColor(0, 0, 0)

        if (p3600.state.dialog.text[1]) then
          love.graphics.print(p3600.state.dialog.text[1], 24 + 2 + 2, 2)
        end

        if (p3600.state.dialog.text[2]) then
          love.graphics.print(p3600.state.dialog.text[2], 24 + 2 + 2, 2 + 8)
        end

        if (p3600.state.dialog.text[3]) then
          love.graphics.print(p3600.state.dialog.text[3], 24 + 2 + 2, 2 + 8 + 8)
        end

        if (p3600.state.dialog.lock) and (#p3600.state.dialog.text <= 3) then
          love.graphics.setColor(255, 242, 230)
          love.graphics.rectangle('fill', 0, (8 * 3) + 4, 320, (8 * 3) + 4,
                                  5, 5)
          love.graphics.setColor(0, 0, 0)
          love.graphics.rectangle('line', 0, (8 * 3) + 4, 320, (8 * 3) + 4,
                                  5, 5)

          local s = p3600.state.dialog.selection

          if (p3600.state.dialog.choices[s - 1]) then
            love.graphics.print(p3600.state.dialog.choices[s - 1].label,
                                2, (8 * 3) + 4 + 2)
          end

          love.graphics.print(p3600.state.dialog.choices[s].label,
                              2 + 8, (8 * 3) + 4 + 2 + 8)

          if (p3600.state.dialog.choices[s + 1]) then
            love.graphics.print(p3600.state.dialog.choices[s + 1].label,
                                2, (8 * 3) + 4 + 2 + 8 + 8)
          end
        end
      end

      p3600.display.changed = true
      p3600.state.changed = false
      love.graphics.setColor(255, 255, 255)
    end
  end
end
