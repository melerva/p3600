local random = require('math').random

local FOREST_MAXX = 30
local FOREST_MAXY = 30

local grass_ids = {
  1,
}

local tree_ids = {
  2,
}

return function(x, y)
  local params = {
    tileset = 'bg0',
    height = 30,
    width = 30,
    default_entrance = {
      player = {
        x = 10,
        y = 10,
        dir = 0,
      },
      followers = {
        x = 11,
        y = 10,
        dir = 0,
      },
    },
    exits = {},
  }

  if (x < FOREST_MAXX) then
    params.exits.right = 'forest_'..tostring(x + 1)..'_'..tostring(y)
  else
    params.exits.right = 'forest_2_'..tostring(y)
  end

  if (x > 1) then
    params.exits.left = 'forest_'..tostring(x - 1)..'_'..tostring(y)
  else
    params.exits.left = 'forest_'..tostring(FOREST_MAXX - 1)..'_'..tostring(y)
  end

  if (y < FOREST_MAXY) then
    params.exits.bottom = 'forest_'..tostring(x)..'_'..tostring(y + 1)
  else
    params.exits.bottom = 'forest_'..tostring(x)..'_2'
  end

  if (y > 1) then
    params.exits.top = 'forest_'..tostring(x)..'_'..tostring(y - 1)
  else
    params.exits.top = 'forest_'..tostring(x)..'_'..tostring(FOREST_MAXY - 1)
  end

  params.bg = {}

  for y = 1, 30, 1 do
    params.bg[y] = {}

    for x = 1, 30, 1 do
      params.bg[y][x] = grass_ids[random(#grass_ids)]
    end
  end

  params.fg = {}
  for y = 1, 30, 1 do
    params.fg[y] = {}

    if (y == 1) then
      for x = 1, 30, 1 do
        params.fg[1][x] = 0
      end
    elseif (y == 30) then
      for x = 1, 30, 1 do
        params.fg[30][x] = 0
      end
    else
      for x = 1, 30, 1 do
        if (x == 1) then
          params.fg[y][1] = 0
        elseif (x == 30) then
          params.fg[y][30] = 0
        else
          if (random() < 0.2) then
            params.fg[y][x] = tree_ids[random(#tree_ids)]
          else
            params.fg[y][x] = 0
          end
        end
      end
    end
  end

  params.fg[10][10] = 0
  params.fg[10][11] = 0

  params.tiletypes = {}

  for y, r in pairs(params.fg) do
    params.tiletypes[y] = {}

    for x, v in pairs(r) do
      if (v == 0) then
        params.tiletypes[y][x] = 0
      else
        params.tiletypes[y][x] = 1
      end
    end
  end

  return params
end
