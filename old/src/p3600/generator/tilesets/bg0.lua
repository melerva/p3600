return {
  name = "bg0",
  firstgid = 1,
  tilewidth = 16,
  tileheight = 16,
  spacing = 0,
  margin = 0,
  image = "../../data/spritesheet/bg0.png",
  imagewidth = 256,
  imageheight = 256,
  tileoffset = {
    x = 0,
    y = 0
  },
  properties = {},
  terrains = {},
  tilecount = 6,
  tiles = {}
}
