The majority of this work, unless otherwise stated, is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit <http://creativecommons.org/licenses/by-sa/3.0> or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

## Credits

 - `/assets/i/font.ttf` is from <https://int10h.org/oldschool-pc-fonts/fontlist/#ibm_pc_bios>, licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0).

Most art and textures are from the [Liberated Pixel Cup](https://lpc.opengameart.org), the rest are either edits or original work made specifically for this game, and should be considered to share the same license.

 - `/assets/i/tiles/forest`: <https://opengameart.org/content/lpc-forest-tiles>
