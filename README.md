# p3600

It's a game I'm making.

## Building

Requires the [`zip`](https://en.wikipedia.org/wiki/Info-ZIP) command available on most unix distributions.

1. Run make.

## Running

The built game is a single file: `p3600.love`.
You use [L&Ouml;VE](https://love2d.org) to run that file.

That's it; it's really that simple.
Just the one file, plus L&Ouml;VE which is also super simple.

 - On Linux, just run `love p3600.love`
 - On Windows, once you install L&Ouml;VE you just double-click on p3600.love
   * You can also use the portable version, in which case you just drag p3600.love over love.exe

## TODO

 - Lots of stuff!
 - Learn how to make art.
 - Add sound.
